{
  description = "A smart way to configure river keybindings";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    # imports for following
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    crane = {
      url = "github:ipetkov/crane";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-compat.follows = "flake-compat";
        flake-utils.follows = "flake-utils";
        rust-overlay.follows = "rust-overlay";
      };
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs = {};
    };
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
  };

  outputs = {
    self,
    nixpkgs,
    crane,
    flake-utils,
    rust-overlay,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [(import rust-overlay)];
      };
      craneLib = (crane.mkLib pkgs).overrideToolchain pkgs.rust-bin.stable.latest.minimal;

      craneBuild = craneLib.buildPackage {
        src = craneLib.cleanCargoSource ./.;

        doCheck = true;
      };
    in {
      packages.default = craneBuild;

      app.default = {
        type = "app";
        program = "${self.packages.${system}.default}/bin/river_init_lesser";
      };

      devShells.default = pkgs.mkShell {
        packages =
          (builtins.attrValues {
            inherit
              (pkgs)
              nil
              alejandra
              statix
              rust-analyzer
              ltex-ls
              cargo-edit
              ;
          })
          ++ [pkgs.rust-bin.stable.latest.default];
      };
    });
}
# vim: ts=2

