use clap::Parser;
use serde::{Deserialize, Serialize};
use std::fs;
use std::path::PathBuf;
use std::process::Command;

/// An easy tool to manage your mappings for the river window manager
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Path to mappings file
    #[clap(value_parser, required = true)]
    path: PathBuf,
}

#[derive(Copy, Deserialize, Serialize, Debug, Clone)]
enum MapMode {
    Map,
    MapMouse,
    Unmap,
}
impl<'a> MapMode {
    fn get_value(self) -> &'a str {
        match self {
            MapMode::Map => return "map",
            MapMode::MapMouse => return "map-pointer",
            MapMode::Unmap => return "unmap",
        }
    }
}

#[derive(Deserialize, Serialize, Debug)]
struct RiverctlCommand {
    map_mode: MapMode,
    mods: String,
    key: String,
    command: String,
    mode: Vec<String>,
    command_args: Option<String>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
struct RiverctlTagCommand {
    map_mode: MapMode,
    mods: String,
    command: String,
    mode: Vec<String>,
}

#[derive(Debug, Deserialize, Serialize)]
struct RiverctlCommandArray {
    commands: Vec<RiverctlCommand>,
    tags_number: u32,
    tag_commands: Vec<RiverctlTagCommand>,
}

//const KEY_INPUT_FILE: &str = "/home/dt/.config/river/res/keys.ron";

fn main() {
    let mut args = Args::parse();
    args.path = fs::canonicalize(args.path).expect("Path could not be canonicalized");
    let commands = fs::read_to_string(args.path).expect("Input does not exist");
    match ron::from_str(&commands) {
        Err(e) => {
            eprintln!(
                "Error: {}, at line: {}, col: {}",
                e.code, e.position.line, e.position.col
            );
        }
        Ok(a) => {
            let command_array: RiverctlCommandArray = a;
            //println!("{:#?}", command_array);

            for i in command_array.commands {
                set_key_mapping(i);
            }

            for i in 1..=command_array.tags_number {
                let tag = 1 << (i - 1);
                for a in command_array.tag_commands.clone() {
                    let command = RiverctlCommand {
                        map_mode: a.map_mode,
                        mods: a.mods,
                        key: i.to_string(),
                        command: a.command,
                        mode: a.mode,
                        command_args: Some(tag.to_string()),
                    };
                    set_key_mapping(command);
                }
            }
        }
    }
}

fn set_key_mapping(i: RiverctlCommand) {
    for a in i.mode {
        let mut command = Command::new("riverctl");
        command.arg(i.map_mode.get_value());
        command.arg(a);
        command
            .arg(i.mods.clone())
            .arg(i.key.clone())
            .arg(i.command.clone());
        if let Some(e) = i.command_args.clone() {
            command.arg(e);
        }

        command.spawn().expect("Should work");
    }
}

//let a = ron::ser::PrettyConfig::new()
//    .depth_limit(10)
//    .new_line("\n".to_owned())
//    .indentor( "  ".to_owned())
//    .separator( " ".to_owned())
//    .struct_names( true)
//    .separate_tuple_members( true)
//    .enumerate_arrays( true)
//    .compact_arrays( true)
//    .extensions( ron::extensions::Extensions::all());

//let b = ron::ser::to_string_pretty(&commands, a).unwrap();
//println!("{b}");
